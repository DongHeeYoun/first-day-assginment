import csv

# Read lines from csv
f = open('users.csv', 'r')
last_names = {}
rdr = csv.reader(f)

# Count overlapped last_name
for last_name in rdr:
    name_num = last_names.get(last_name[1],0)
    last_names[last_name[1]] = name_num+1

# Print last_names dict
print(last_names)

f.close()
