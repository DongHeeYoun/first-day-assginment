import csv

# Get user information
def get_info():
    result = {}
    print("Input name")
    name = raw_input()
    first_name, last_name = name.split()
    result["fname"] = first_name
    result["lname"] = last_name    
    
    print("input email address")
    email_address = raw_input()
    result["email"] = email_address    

    print("input phone number")
    phone = raw_input()
    result["phone_num"] = phone

    print("Thank you!!")
    return result

# Save it in csv file
def save_info(file_name, info):
    f = open(file_name, 'a')
    wr =  csv.writer(f)
    wr.writerow([info["fname"], info["lname"], info["email"], info["phone_num"]])
    f.close()


info = get_info()
save_info('users.csv', info)
