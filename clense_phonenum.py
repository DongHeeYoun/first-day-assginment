import csv
import re

# read and replace by regex
def clense_phonenums(file_name):
    f = open(file_name,'r')
    rdr = csv.reader(f)
    lines = []
    for line in rdr:
        line[3] = re.sub(r"[^0-9]","-",line[3])
        lines.append(line)
    f.close()
    return lines

# Update csv file
def update_file(lines):
    f = open('users.csv', 'w')
    wr = csv.writer(f)
    wr.writerows(lines)
    f.close()
# print results
def print_clensed(lines):
    print("Clensed phone numbers")
    for line in lines:
        print(line)

lines = clense_phonenums('users.csv')
print_clensed(lines)
