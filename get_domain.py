import csv

# Get domains from file
def get_domains(file_name):
    lines = []    
    f = open(file_name, 'r')
    rdr = csv.reader(f)
    for line in rdr:
        lines.append(line)
    f.close()
    return lines
    
# Clense All domains by split by @
def clense_domains(lines):
    clensed = []
    for line in lines:
        email_head, email_tail = line[2].split('@')
        clensed.append([email_head, email_tail])
    return clensed

# Print Clensed result
def print_clensed(clensed):
    print("Clensed Domains")
    for line in clensed:
        print("Output : ",line[0], " : ", line[1])

lines = get_domains('users.csv')
clensed = clense_domains(lines)
print_clensed(clensed)
